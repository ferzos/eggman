![](logo.png)

# Eggman

Eggman is web service that serve data from mLab database. Database contains notebooks data, populated from enterkomputer API [https://enterkomputer.com/api/product]

### Prerequisites

```
node js
```

### Installing

```
touch .env (please ask author for the content)
npm install
```

## Built With

* [Express](http://expressjs.com/) - Framework
* [Mongoose](http://mongoosejs.com/) - Object modeling


## Authors

* **Ferzos**
